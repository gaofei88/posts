import React from 'react';
import Posts from './components/Posts';
import { Provider } from 'react-redux';
import { store } from './redux/store';


export default function App() {
  return (
    <Provider store={store}>
      <div className="App" >
        <Posts />
      </div>
    </Provider>
  );
}