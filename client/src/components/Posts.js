/* eslint-disable react/prop-types */
import React from 'react';
import {  CircularProgress, Fab } from '@material-ui/core';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';

import { prevPage, nextPage } from '../redux/actions/PostsActions';
import Post from './Post';

const styles = makeStyles((theme) => ({
    fabPrevStyle: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 120,
        left: 'auto',
        position: 'fixed'
    },
    fabPageNumStyle: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 70,
        left: 'auto',
        position: 'fixed'
    },
    fabNextStyle: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 20,
        left: 'auto',
        position: 'fixed'
    }
}));

function Posts({ isLoading, curPage, posts, goNextPage, goPrevPage }) {
    const classes = styles();

    return (
        <div>
            { isLoading && <CircularProgress />}
            {
                posts.map(each =>
                    <Post post={ each } key={ each.id } classes={ classes }/>   
                )
            }
            <Fab size="small" color="primary" className={ classes.fabPageNumStyle }  aria-label="edit">
                {curPage}
            </Fab>
            <Fab size="small" onClick={ goPrevPage } color="secondary" className={ classes.fabPrevStyle } aria-label="add">
                <NavigateBeforeIcon />
            </Fab>
            <Fab size="small" onClick={ goNextPage } color="secondary" className={ classes.fabNextStyle }  aria-label="edit">
                <NavigateNextIcon />
            </Fab>
        </div>
    )
}

const mapStateToProps = (state) => ({
    isLoading: state.Posts.isLoading,
    posts: state.Posts.postsToView,
    page: state.Posts.currentPage,
    curPage: state.Posts.currentPage
})

const mapDispatchToProps = (dispatch) => ({
    goNextPage: () => (dispatch(nextPage())),
    goPrevPage: () => (dispatch(prevPage()))
})

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
