/* eslint-disable react/prop-types */
import React, { useRef, useState, useEffect } from 'react';
import { Avatar, Card, CardHeader, CardContent, CircularProgress, Collapse, IconButton, Typography } from '@material-ui/core';
import SpeakerNotesIcon from '@material-ui/icons/SpeakerNotes';
import { makeStyles } from '@material-ui/core/styles';
import { apiBaseUrl } from '../config';
import Comments from './Comments';

const styles = makeStyles((theme) => ({
    post: {
        marginBottom: 5
    }
}));

export default function Post({ post }) {
    const myRef = useRef();
    const classes = styles();
    const [ expanded, setExpanded ] = useState(false);
    const [ comments, setComments ] = useState([]);
    const [ isLoading, setLoading ] = useState(true);
    const [ errorMsg, setErrorMsg ] = useState(null);

    useEffect(() => {
        if ( expanded ) {
            // eslint-disable-next-line no-undef
            window.scrollTo(0, myRef.current.offsetTop - 10)
        }
    })

    const handleExpansion = ()=> {
        setExpanded(!expanded);
        if (expanded || comments.length > 0) {
           return;
        }
        fetch(`${ apiBaseUrl }posts/${ post.id }/comments`)
        .then(res => res.json())
        .then(commentsRes => {
                setLoading(false)
                setComments(commentsRes)
            }, error => { 
                setLoading(false)
                setErrorMsg("There's error fetching the comments, please try again later.")
                // eslint-disable-next-line no-undef
                console.log(error)
            });
    }
 
    return (
        <Card variant="outlined" className={ classes.post } ref={ myRef }>
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe">
                        { post.username.substr(0,1) }
                    </Avatar>
                }
                title={ post.title }
                subheader={ post.username }
                action={
                    <IconButton 
                        onClick={ handleExpansion }
                        aria-expanded={ expanded }
                        aria-label="comments">
                        <SpeakerNotesIcon />
                    </IconButton>
                }
            />
            <CardContent> 
                {
                    post.body.split('\n').map((line, index) => 
                        // eslint-disable-next-line react/no-array-index-key
                        <Typography key={ index }>{line}</Typography>
                    )
                }
            </CardContent>
            <Collapse in={ expanded } timeout="auto" unmountOnExit>  
                <CardContent> 
                    { isLoading && <CircularProgress />}
                    { errorMsg !== null && <p>{errorMsg}</p>}
                    <Comments comments={ comments }/>
                </CardContent>
            </Collapse> 
        </Card>
    )
    
}