/* eslint-disable react/prop-types */
import React from 'react';
import { Divider, Grid, Typography, List, ListItem, ListItemText } from '@material-ui/core';

function CommentAuthor({ name, email }) {
    return (
        <Grid container component="span" alignContent="space-between">
            <Grid item xs={ 12 } sm={ 8 } component="span">{ name }</Grid>
            <Grid item xs={ 12 } sm={ 4 } component="span">{email}</Grid>
        </Grid>
    )
}

export default function Comments({ comments }) {
    return (
        <List>
            {
                comments.map((each,index) => 
                    // eslint-disable-next-line react/no-array-index-key
                    <div key={ `${ each.id }-${ index }` }>
                        <Divider/>
                        <ListItem >
                            <ListItemText 
                            primary={
                                <Typography paragraph align="justify">
                                    { each.body }
                                </Typography>
                            }
                            secondary={
                                <CommentAuthor name={ each.name } email={ each.email }/>
                            }/>
                        </ListItem>
                    </div>
                )
            }
        </List>
    )
}