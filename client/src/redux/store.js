import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { getPosts } from './actions/PostsActions';

import { combineReducers } from 'redux';
import Posts from './reducers/PostsReducer';

const store = createStore(
    combineReducers({ Posts }),
    applyMiddleware(ReduxThunk)
);

store.dispatch(getPosts());

export { store }