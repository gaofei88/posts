import { createAction } from 'redux-actions';
import { apiBaseUrl } from '../../config';

const GET_POSTS = 'GET_POSTS';
const SHOW_PAGE = 'SHOW_PAGE';

const getPostsAction = createAction(GET_POSTS)
const showPageAction = createAction(SHOW_PAGE);

const getPosts = () => {
    return (dispatch, getState) => {
        // eslint-disable-next-line no-undef
        fetch(`${ apiBaseUrl }posts`)
        .then(res => res.json())
        .then(
            posts => dispatch(getPostsAction(posts)),
            // eslint-disable-next-line no-undef
            error => {
                alert("There's a problem with the network.");
                console.error(error)
            }
        );
    }
}

const prevPage = () => {
    return (dispatch, getState) => {
        const postState = getState().Posts;
        dispatch(showPageAction(postState.currentPage - 1));
    }
}

const nextPage = () => {
    return (dispatch, getState) => {
        const postState = getState().Posts;
        dispatch(showPageAction(postState.currentPage + 1));
    }
}

export { getPosts, prevPage, nextPage }