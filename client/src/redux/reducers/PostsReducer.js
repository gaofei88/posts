import { handleActions } from 'redux-actions';

const posts = {
    isLoading: true,
    currentPage: 1,
    posts: [],
    postsToView: []
}

export default handleActions({
    GET_POSTS: (state, { payload }) => {
        const start = (state.currentPage - 1) * 10;
        return {
            isLoading: false,
            posts: payload,
            currentPage: 1,
            postsToView: payload.slice(start, start + 10)
        }
    },
    SHOW_PAGE: (state, { payload }) => {
        if (state.currentPage === payload || payload <= 0) {
            return state;
        } else {
            const start = (payload - 1) * 10;
            if (start >= state.posts.length) {
                return state;
            } else {
                return Object.assign({}, state, {
                    currentPage: payload,
                    postsToView: state.posts.slice(start, start + 10)
                })
            }
        }
    }
}, posts)