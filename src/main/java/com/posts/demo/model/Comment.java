package com.posts.demo.model;

import lombok.Data;

@Data
public class Comment {
	private Integer postId;
	private String name;
	private String email;
	private String body;
}
