package com.posts.demo.config;

import com.posts.demo.service.CommentsService;
import com.posts.demo.service.PostsService;
import com.posts.demo.service.UsersService;
import com.posts.demo.util.ServiceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig {

	@Autowired
	ServiceGenerator serviceGenerator;

	@Bean
	public CommentsService getCommentsService(ServiceGenerator generator) {
		return generator.createService(CommentsService.class);
	}

	@Bean
	public PostsService getPostsService(ServiceGenerator generator) {
		return generator.createService(PostsService.class);
	}

	@Bean
	public UsersService getUsersService(ServiceGenerator generator) {
		return generator.createService(UsersService.class);
	}

	//TODO: use profile to control
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurer() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry.addMapping("/**/*")
//						.allowedOrigins("*");
//			}
//		};
//	}
}
