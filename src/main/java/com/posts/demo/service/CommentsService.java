package com.posts.demo.service;

import com.posts.demo.model.Comment;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface CommentsService {

	@GET("/comments")
	Call<List<Comment>> getCommentsByPostId(@Query("postId") Integer postId);

}
