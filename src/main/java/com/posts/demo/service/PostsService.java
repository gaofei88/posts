package com.posts.demo.service;

import com.posts.demo.model.Post;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface PostsService {

	@GET("/posts")
	Call<List<Post>> getPosts();
}
