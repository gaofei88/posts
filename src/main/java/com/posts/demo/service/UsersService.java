package com.posts.demo.service;

import com.posts.demo.model.User;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface UsersService {
	@GET("/users")
	Call<List<User>> getUsers();
}
