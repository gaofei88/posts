package com.posts.demo.util;

import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

@Component
public class ServiceGenerator {

	private final Retrofit retrofit;

	public ServiceGenerator(@Value("${dataBaseUrl}") String baseUrl) {
		this.retrofit = new Retrofit.Builder()
				.baseUrl(baseUrl)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
	}

	public <T> T createService(Class<T> serviceClass) {
		return retrofit.create(serviceClass);
	}

}
