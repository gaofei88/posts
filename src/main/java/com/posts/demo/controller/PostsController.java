package com.posts.demo.controller;

import com.posts.demo.model.Post;
import com.posts.demo.model.User;
import com.posts.demo.service.PostsService;
import com.posts.demo.service.UsersService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.Call;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
public class PostsController {

	private final PostsService postsService;

	private final UsersService usersService;

	public PostsController(PostsService postsService, UsersService usersService) {
		this.postsService = postsService;
		this.usersService = usersService;
	}

	@GetMapping(path = "/posts")
	public List<Post> getPosts(HttpServletResponse response) {
		Call<List<Post>> postsCall = postsService.getPosts();
		Call<List<User>> usersCall = usersService.getUsers();

		try {
			List<User> users = usersCall.execute().body();
			Objects.requireNonNull(users);
			Map<Integer, String> usersMap = users.stream()
					.collect(Collectors.toMap(User::getId, User::getUsername));

			List<Post> posts = postsCall.execute().body();
			assert posts != null;
			posts.forEach(post ->
				post.setUsername(usersMap.get(post.getUserId()))
			);

			return posts;
		} catch (IOException e) {
			response.setStatus(503);
		}
		return null;
	}

}
