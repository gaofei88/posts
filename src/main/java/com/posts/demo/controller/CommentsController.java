package com.posts.demo.controller;

import com.posts.demo.model.Comment;
import com.posts.demo.service.CommentsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.Call;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class CommentsController {

	private final CommentsService commentsService;

	public CommentsController(CommentsService commentsService) {
		this.commentsService = commentsService;
	}

	@GetMapping("/posts/{postId}/comments")
	public List<Comment> getComments(@PathVariable("postId") Integer postId,
									 HttpServletResponse response) {

		Call<List<Comment>> commentsCall = commentsService.getCommentsByPostId(postId);

		try {
			return commentsCall.execute().body();
		} catch (IOException e) {
			response.setStatus(503);
		}
		return null;

	}
}
