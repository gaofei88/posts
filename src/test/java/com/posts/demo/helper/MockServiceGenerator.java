package com.posts.demo.helper;

import com.posts.demo.service.PostsService;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MockServiceGenerator {

	private final MockRetrofit mockRetrofit;

	public MockServiceGenerator(NetworkBehavior behavior) {

		Retrofit retrofit = new Retrofit.Builder()
				.addConverterFactory(GsonConverterFactory.create())
				.baseUrl("https://x.com") // any url is fine
				.build();

		mockRetrofit = new MockRetrofit.Builder(retrofit)
				.networkBehavior(behavior)
				.build();

	}

	public <T> BehaviorDelegate<T> create(Class<T> serviceClass) {
		Objects.requireNonNull(mockRetrofit);
		return mockRetrofit.create(serviceClass);
	}
}
