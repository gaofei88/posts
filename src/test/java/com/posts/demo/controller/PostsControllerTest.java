package com.posts.demo.controller;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.posts.demo.helper.MockServiceGenerator;
import com.posts.demo.model.Post;
import com.posts.demo.model.User;
import com.posts.demo.service.PostsService;
import com.posts.demo.service.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.NetworkBehavior;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class PostsControllerTest {

	private final NetworkBehavior behavior = NetworkBehavior.create();
	private final Gson gson = new Gson();

	private HttpServletResponse httpResponse;
	private PostsController postsController;

	@BeforeEach
	public void setUp() {
		BehaviorDelegate<PostsService> postDelegate = new MockServiceGenerator(behavior).create(PostsService.class);
		BehaviorDelegate<UsersService> userDelegate = new MockServiceGenerator(behavior).create(UsersService.class);

		List<Post> posts = null;
		List<User> users = null;

		try(InputStream inputStream = PostsControllerTest.class.getClassLoader().getResourceAsStream("posts.json")) {
			assert inputStream != null;
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			posts = gson.fromJson(bufferedReader, new TypeToken<List<Post>>(){}.getType());
			httpResponse = mock(HttpServletResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try(InputStream inputStream = PostsControllerTest.class.getClassLoader().getResourceAsStream("users.json")) {
			assert inputStream != null;
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			users = gson.fromJson(bufferedReader, new TypeToken<List<User>>(){}.getType());
			httpResponse = mock(HttpServletResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		postsController = new PostsController(postDelegate.returningResponse(posts), userDelegate.returningResponse(users));

	}

	@Test
	public void testGetPostsSuccessfully() {
		behavior.setFailurePercent(0);

		List<Post> results = postsController.getPosts(httpResponse);

		assertEquals(5, results.size());
	}

	@Test
	public void testShouldReturn503WhenRemoteCallFails() {
		behavior.setFailurePercent(100);

		List<Post> results = postsController.getPosts(httpResponse);

		assertNull(results);
	}
}
