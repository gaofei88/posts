package com.posts.demo.controller;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.posts.demo.helper.MockServiceGenerator;
import com.posts.demo.model.Comment;
import com.posts.demo.model.Post;
import com.posts.demo.service.CommentsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.NetworkBehavior;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;

public class CommentsControllerTest {

	private final NetworkBehavior behavior = NetworkBehavior.create();
	private final Gson gson = new Gson();

	private HttpServletResponse httpResponse;
	private CommentsController commentsController;

	@BeforeEach
	public void setUp() {

		BehaviorDelegate<CommentsService> delegate = new MockServiceGenerator(behavior).create(CommentsService.class);

		httpResponse = mock(HttpServletResponse.class);

		try(InputStream inputStream = PostsControllerTest.class.getClassLoader().getResourceAsStream("comments.json");) {
			assert inputStream != null;
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			List<Comment>  comments = gson.fromJson(bufferedReader, new TypeToken<List<Post>>(){}.getType());
			commentsController = new CommentsController(delegate.returningResponse(comments));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testShouldGetCommentsSuccessfully() {
		behavior.setFailurePercent(0);

		List<Comment> results = commentsController.getComments(1,httpResponse);

		assertEquals(results.size(), 5);
	}

	@Test
	public void testShouldReturnNullWhenNetworkFails() {
		behavior.setFailurePercent(100);

		List<Comment> results = commentsController.getComments(1, httpResponse);

		assertNull(results);
	}

}
