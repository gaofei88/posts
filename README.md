# Posts

This project runs with **maven** and **jdk 8**
client is built with **ReactJS**

## Quick Start
This repo utilises GitLab CI and you can download a [pre-built **jar**](https://gitlab.com/gaofei88/posts/-/jobs/artifacts/master/download?job=build) 

## Run
run `java -jar xxxx.jar` (xxx.jar can be found in )
hit `http://localhost:8080` in browser

## Build
run `mvn clean package` (client will be built together)
